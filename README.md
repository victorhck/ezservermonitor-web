[eZ Server Monitor](http://www.ezservermonitor.com) (eSM) es un script para el monitorizado de servidores de GNU/Linux. Está disponible en una versión en [Bash](http://www.ezservermonitor.com/esm-sh/features) y una aplicación [Web](http://www.ezservermonitor.com/esm-web/features)

# eZ Server Monitor `Web

En su versión [Web](http://www.ezservermonitor.com/esm-web/features) eSM es un script en PHP que muestra en una página web información como:

![](http://www.ezservermonitor.com/uploads/esm_web/esm-web_dashboard-complete.png)

- **Sistema** : hostname, sistema operativo, versión del kernel, uptime, último reinicio, número de usuario actuales, fecha y hora del servidor
- **Carga media** : unas gráficas muestran la carga de la CPU en porcentaje (1 minuto, 5 minutos y 15 minutos)
- **Uso de la red** : muestra la dirección IP de cada interfaz de red con los datos enviados y recibidos
- **CPU** : modelo, frecuencia, número de núcleos, cache L2, bogomips, temperatura
- **Uso del disco** : tabla con cada punto de montaje con el espacio disponible, utilizado y total
- **Memoria** : tabla que contiene la cantidad de memoria RAM disponible, utilizada y total
- **Swap** : tabla que contiene la cantidad de memoria Swap disponible, utilizada y total
- **Último inicio de sesión** : muestra las últimas 5 conexiones
- **Ping** : ping a las direcciones definidas en el archivo de configuración
- **Servicios** : muestra el estado (disponible o caido) de servicios definidos en el archivo de configuración

¡Muchos temas disponibles!

![](http://www.ezservermonitor.com/uploads/esm_web/esm-web_themes.png)

Cada bloque puede ser refrescado de manera independiente de forma manual.

Puedes descargar la última versión desde [aquí](http://www.ezservermonitor.com/esm-web/downloads). Los [requisitos](http://www.ezservermonitor.com/esm-web/documentation) son simples : un entorno Linux, un servidor web (Apache2, Lighttpd, Nginx, ...) y PHP.

La [documentación](http://www.ezservermonitor.com/esm-web/documentation) explica todos los parámetros de *esm.config.json*.

Changelog está disponible [aquí](http://www.ezservermonitor.com/esm-web/changelog).

**Puedes ver más información en el [sitio web oficial](http://www.ezservermonitor.com/esm-web/features).**

---

Aquí lo he clonado, para traducirlo, reordenarlo a mi gusto y adaptarlo a openSUSE. Puedes leer el artículo que escribí [en mi blog](https://victorhckinthefreeworld.com/2018/02/22/monitoriza-el-rendimiento-de-tus-servidores-con-ez-server-monitor/)

![](https://victorhckinthefreeworld.files.wordpress.com/2018/02/ez_server_es.png)
